RocketChat.models.Votes = new class extends RocketChat.models._Base
    constructor: ->
        @_initModel 'vote'

        @tryEnsureIndex { 't': 1 }
        @tryEnsureIndex { 'rid': 1 }

    # Supported fields:
    # _id
    # rid: room id
    # t: type
    # public: are the votes hidden or not?
    # _createdAt
    # description
    # target: target of the ban (only for t: voteban)


    # FIND ONE
    findOneById: (_id, options) ->
        query =
            _id: _id

        return @findOne query, options

    findOneByTypeTargetAndRoom: (type, target, rid, options) ->
        query =
            t: type
            rid: rid
            target: target

        return @findOne query, options

    # FIND
    findById: (_id, options) ->
        query =
            _id: _id

        return @find query, options

    findByType: (type, options) ->
        query =
            t: type

        return @find query, options

    findOpenByType: (type, options) ->
        query =
            t: type
            closed: false

        return @find query, options


    # UPDATE
    setDescriptionById: (_id, description) ->
        query =
            _id: _id

        update =
            $set:
                description: description

        return @update query, update

    voteById: (_id, username, choice) ->
        # TODO: check here if choice is a valid choice?
        query =
            _id: _id

        update =
            $addToSet:
                choices:
                    '#{choice}': username

        return @update query, update


    closeById: (_id) ->
        query =
            _id: _id

        update =
            $set:
                closed: true

        return @update query, update

    # INSERT
    createWithTypeAndRoom: (type, rid, extraData) ->
        vote =
            t: type
            rid: rid
            _createdAt: new Date
            closed: false
            public: true

        _.extend vote, extraData

        vote._id = @insert vote
        return vote

    # REMOVE
    removeById: (_id, options) ->
        query =
            _id: _id

        return @remove query, options
