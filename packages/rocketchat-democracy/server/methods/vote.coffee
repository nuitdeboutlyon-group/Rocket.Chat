RockeChat.democracy.vote = voteId, choice ->
    user = Meteor.userId()
    unless user
        throw new Meteor.Error 'error-invalid-user', 'Invalid user', { method: 'vote' }¬

    vote = RocketChat.models.Vote.findOneById voteId

    unless vote
        throw new Meteor.Error 'invalid-vote', { method: 'vote' }

    if vote.closed
        throw new Meteor.Error 'vote-closed', { method: 'vote' }

    if user.username in vote.choices[choice] # TODO: test if it works, else try choices.'{#choice}'
        throw new Meteor.Error 'user-already-voted', { method: 'vote' }

    vote = RocketChat.models.Vote.voteById vote._id, user, choice

    RocketChat.
