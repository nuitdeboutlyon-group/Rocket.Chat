class VoteBan
	constructor: (command, params, item) ->
		if command isnt 'voteban' or not Match.test params, String
			return

		username = params.trim()
		if username is ''
			return

		username = username.replace('@', '')

		target = Meteor.users.findOneByUsername username
		currentUser = Meteor.users.findOne Meteor.userId()
		room = RocketChat.models.Rooms.findOneById item.rid

		if not target?
			console.log 'notify user_doesnt_exist'
			RocketChat.Notifications.notifyUser Meteor.userId(), 'message', {
				_id: Random.id()
				rid: item.rid
				ts: new Date
				msg: TAPi18n.__('User_doesnt_exist', { postProcess: 'sprintf', sprintf: [ target ] }, currentUser.language)
			}
			return

		vote = RocketChat.democracy.uniqueVoteBan rid, target._id

		try
			RocketChat.democracy.vote vote._id, 'for'
		catch e
			if e.error is 'user-already-voted'
				RocketChat.Notifications.notifyUser Meteor.userId(), 'message', {
					_id: Random.id()
					rid: item.rid
					ts: new Date
					msg: TAPi18n.__('User_already_votebanned', { postProcess: 'sprintf', sprintf: [ currentUser.username, target.username ] }, currentUser.language)
				}
				return
			else
				RocketChat.Notifications.notifyUser Meteor.userId(), 'message', {
					_id: Random.id()
					rid: item.rid
					ts: new Date
					msg: TAPi18n.__(e.error, null, currentUser.language)
				}
			return

		return

RocketChat.slashCommands.add 'voteban', VoteBan

