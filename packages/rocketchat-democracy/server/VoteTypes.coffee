RocketChat.VoteTypes = new class
	types = {}

	registerType = (options) ->
		types[options.id] = options

	getType = (vote) ->
		return types[vote?.t]

	registerType: registerType
	getType: getType


Meteor.startup ->
	RocketChat.VoteTypes.registerType
		id: 'voteban'
		close: (vote) ->
			return false #TODO
		action: (vote) ->
			if vote.result is 'for'
				# TODO: kicking is a server method, how to call it as a
				# 'system' user? (like a function).
			else
				return

	RocketChat.VoteTypes.registerType
		id: ''
