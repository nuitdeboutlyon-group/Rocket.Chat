RocketChat.democracy.uniqueVoteBan = (targetId, rid) ->
    unless targetId and rid
        return false

    target = RocketChat.models.Users.findOneById targetId
    unless target
        throw new Meteor.Error 'invalid-user'

    voteBan = RocketChat.models.Votes.findOneByTypeTargetAndRoom 'voteban', targetId, rid
    if not voteBan or voteBan.closed
        voteBan = RocketChat.models.Votes.createWithTypeAndRoom 'voteban', rid, {target: targetId}

    return voteBan
