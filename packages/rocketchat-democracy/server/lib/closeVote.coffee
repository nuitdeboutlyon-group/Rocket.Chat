RocketChat.democracy.closeVote = (vid) ->
    vote = RocketChat.models.Vote.findOneById vid
    unless vote
        throw new Meteor.Error 'invalid-vote'
    if vote.closed
        throw new Meteor.Error 'vote-closed'

    RocketChat.models.Vote.closeById vid
    RocketChat.callbacks.call 'closevote-'+vote.t

    return true
