Package.describe({
	name: 'rocketchat:democracy',
	version: '0.0.1',
	summary: 'Some helpers to build a democratic moderation',
	git: ''
});

Package.onUse(function(api) {

	api.versionsFrom('1.0');

	api.use([
		'coffeescript',
		'check',
		'rocketchat:lib'
	]);

});
