Meteor.methods
    hasRole: (userId, roleNames, scope) ->
        unless Meteor.userId()
            throw new Meteor.Error 'error-invalid-user', 'Invalid user', { method: 'hasRole' }
        return RocketChat.authz.hasRole userId, roleNames, scope
